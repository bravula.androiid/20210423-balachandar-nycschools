package com.idroidz.domain.usecase


import com.idroidz.domain.model.School
import com.idroidz.domain.repository.SchoolRepository
import com.idroidz.domain.util.asDomainModel
import javax.inject.Inject

/**
 * GetSchoolsListUseCase
 *
 * Used to retrieve the list of schools and return the values in a Result object
 * @param schoolRepository repository to get the list from
 */
class GetSchoolsListUseCase @Inject constructor(private val schoolRepository: SchoolRepository) {
    sealed class Result {
        object Loading : Result()
        data class Success(val schoolsList: List<School>?) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    suspend fun getSchoolList(): Result {
        return try {
            Result.Success(schoolRepository.getSchoolsList()?.asDomainModel())
        } catch (ex: Exception) {
            Result.Failure(ex)
        }
    }
}
