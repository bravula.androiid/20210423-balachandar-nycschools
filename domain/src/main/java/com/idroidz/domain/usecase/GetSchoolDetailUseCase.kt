package com.idroidz.domain.usecase

import com.idroidz.domain.model.SchoolDetail
import com.idroidz.domain.repository.SchoolRepository
import com.idroidz.domain.util.asDomainModel
import javax.inject.Inject

/**
 * GetSchoolDetailUseCase
 *
 * Used to retrieve the school detail  and return the values in a Result object
 * @param schoolRepository repository to get the list from
 */
class GetSchoolDetailUseCase @Inject constructor(private val schoolRepository: SchoolRepository) {
    sealed class Result {
        object Loading : Result()
        data class Success(val data: SchoolDetail) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    suspend fun getSchoolDetail(schoolID: String): Result {
        return try {
            Result.Success(schoolRepository.getSchoolDetails(schoolID).asDomainModel())
        } catch (ex: Exception) {
            Result.Failure(ex)
        }
    }
}