package com.idroidz.domain.util

import com.idroidz.domain.model.NetworkSchool
import com.idroidz.domain.model.NetworkSchoolDetail
import com.idroidz.domain.model.School
import com.idroidz.domain.model.SchoolDetail


/**
 * Map network entities to domain entities
 */
fun List<NetworkSchool>.asDomainModel(): List<School> {
    return map {
        School(
                id = it.id,
                schoolName = it.schoolName,
                schoolDescription = it.schoolDescription,
                city = it.city,
                zipCode = it.zipCode
        )
    }
}

fun NetworkSchoolDetail.asDomainModel(): SchoolDetail {
    return SchoolDetail(
            id = id,
            mathScore = mathScore,
            readingScore = readingScore,
            writingScore = writingScore
    )
}

