package com.idroidz.domain.model


import com.google.gson.annotations.SerializedName


data class NetworkSchool(
        @SerializedName("dbn") val id: String,
        @SerializedName("school_name") val schoolName: String,
        @SerializedName("academicopportunities1") val schoolDescription: String?,
        val city: String?,
        @SerializedName("zip") val zipCode: String?
)