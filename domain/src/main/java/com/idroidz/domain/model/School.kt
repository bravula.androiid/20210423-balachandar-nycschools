package com.idroidz.domain.model

import java.io.Serializable

/** Domain model Class required by UI components .
 */
data class School(
        val id: String,
        val schoolName: String,
        val schoolDescription: String?,
        val city: String?,
        val zipCode: String?
) : Serializable