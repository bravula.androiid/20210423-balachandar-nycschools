package com.idroidz.domain.model

/** Domain model Class required by UI components .
 */
data class SchoolDetail(val id: String,
                        val readingScore: String,
                        val mathScore: String,
                        val writingScore: String)