package com.idroidz.domain.model

import com.google.gson.annotations.SerializedName

data class NetworkSchoolDetail(
        @SerializedName("dbn") val id: String,
        @SerializedName("sat_critical_reading_avg_score") val readingScore: String,
        @SerializedName("sat_math_avg_score") val mathScore: String,
        @SerializedName("sat_writing_avg_score") val writingScore: String)