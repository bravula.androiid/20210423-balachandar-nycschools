package com.idroidz.domain.repository

import com.idroidz.domain.model.NetworkSchoolDetail
import com.idroidz.domain.model.NetworkSchool


interface SchoolRepository {

    suspend fun getSchoolsList(): List<NetworkSchool>?

    suspend fun getSchoolDetails(schoolId: String): NetworkSchoolDetail


}
