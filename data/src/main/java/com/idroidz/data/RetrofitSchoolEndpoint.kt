package com.idroidz.data


import com.idroidz.domain.model.NetworkSchool
import com.idroidz.domain.model.NetworkSchoolDetail

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitSchoolEndpoint {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolsList(): List<NetworkSchool>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolDetail(@Query("dbn") schoolID: String): List<NetworkSchoolDetail>
}
