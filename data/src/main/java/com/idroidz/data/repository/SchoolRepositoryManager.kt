package com.idroidz.data.repository

import com.idroidz.data.ApiManager
import com.idroidz.domain.model.NetworkSchoolDetail
import com.idroidz.domain.model.NetworkSchool
import com.idroidz.domain.repository.SchoolRepository
import javax.inject.Inject

class SchoolRepositoryManager @Inject constructor(private val apiManager: ApiManager) : SchoolRepository {

    override suspend fun getSchoolsList(): List<NetworkSchool> {
        return apiManager.getSchoolsList()
    }

    override suspend fun getSchoolDetails(schoolId: String): NetworkSchoolDetail {
        return apiManager.getSchoolDetails(schoolId)
    }
}