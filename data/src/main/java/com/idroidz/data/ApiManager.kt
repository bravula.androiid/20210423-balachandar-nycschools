package com.idroidz.data

import com.idroidz.domain.model.NetworkSchoolDetail
import com.idroidz.domain.model.NetworkSchool

interface ApiManager {
    suspend fun getSchoolsList(): List<NetworkSchool>

    suspend fun getSchoolDetails(schoolID: String): NetworkSchoolDetail

}