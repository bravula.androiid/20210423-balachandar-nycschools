package com.idroidz.data


import com.idroidz.domain.model.NetworkSchoolDetail
import com.idroidz.domain.model.NetworkSchool
import javax.inject.Inject

class RetrofitApiManager @Inject constructor(private val schoolEndpoint: RetrofitSchoolEndpoint) : ApiManager {

    override suspend fun getSchoolsList(): List<NetworkSchool> {
        return schoolEndpoint.getSchoolsList()
    }

    override suspend fun getSchoolDetails(schoolID: String): NetworkSchoolDetail {
        return schoolEndpoint.getSchoolDetail(schoolID)[0]
    }

}
