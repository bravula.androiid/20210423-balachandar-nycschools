# NYCSchools

Simple school application using Clean architecture

## FEATURES

Swipe to refresh is implemented in home screen for refreshing the schools list

## LIBRARIES USED

* Dagger
* Kotlin Coroutines
* Retrofit
* Glide
* Databinding
* JUnit

