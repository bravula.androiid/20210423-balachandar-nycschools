package com.idroidz.nycschools.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.School
import com.idroidz.domain.usecase.GetSchoolsListUseCase
import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.util.CoroutineContextProvider
import com.idroidz.nycschools.util.TestCoroutineRule
import com.idroidz.nycschools.util.TestingCoroutineContextProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.BDDMockito.given

@RunWith(JUnit4::class)
class SchoolViewModelTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var schoolViewModel: SchoolViewModel
    private var contextProvider: CoroutineContextProvider = TestingCoroutineContextProvider()

    @Mock
    private lateinit var application: SchoolApplication

    @Mock
    lateinit var getSchoolListUseCase: GetSchoolsListUseCase

    @Mock
    lateinit var schoolRouter: SchoolRouter


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        schoolViewModel = SchoolViewModel(application, getSchoolListUseCase, schoolRouter, contextProvider)
    }

    @Test
    fun `getSchoolList returns success response`() {
        testCoroutineRule.runBlockingTest {
            val schoolsList = mutableListOf<School>()
            val result = GetSchoolsListUseCase.Result.Success(schoolsList)
            given(getSchoolListUseCase.getSchoolList()).willReturn(result)

            schoolViewModel.getSchools()
            assertEquals(schoolViewModel.schoolsList, schoolsList)

        }

    }

    @Test
    fun `getSchoolList returns failure response`() {
        testCoroutineRule.runBlockingTest {
            val result = GetSchoolsListUseCase.Result.Failure(Exception())
            given(getSchoolListUseCase.getSchoolList()).willReturn(result)

            schoolViewModel.getSchools()
            assertEquals(schoolViewModel.networkFailure.get(), true)

        }

    }
}