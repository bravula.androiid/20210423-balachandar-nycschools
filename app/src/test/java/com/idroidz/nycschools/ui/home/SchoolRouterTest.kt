package com.idroidz.nycschools.ui.home

import android.app.Activity
import com.nhaarman.mockito_kotlin.anyOrNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.lang.ref.WeakReference
@RunWith(MockitoJUnitRunner::class)
class SchoolRouterTest {
    @Mock
    lateinit var activity: Activity

    private lateinit var schoolRouter: SchoolRouter

    @Before
    fun setUp() {
        schoolRouter = SchoolRouter(WeakReference(activity))
    }

    @Test
    fun `navigate shows school detail screen when route is school detail`() {
        schoolRouter.navigate(SchoolRouter.Route.SCHOOL_DETAIL)

        Mockito.verify(activity).startActivity(anyOrNull())
    }
}