package com.idroidz.nycschools.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.School
import com.idroidz.domain.model.SchoolDetail
import com.idroidz.domain.usecase.GetSchoolDetailUseCase
import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.util.CoroutineContextProvider
import com.idroidz.nycschools.util.TestCoroutineRule
import com.idroidz.nycschools.util.TestingCoroutineContextProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

@RunWith(JUnit4::class)
class SchoolDetailViewModelTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var schoolDetailViewModel: SchoolDetailViewModel
    private var contextProvider: CoroutineContextProvider = TestingCoroutineContextProvider()

    @Mock
    private lateinit var application: SchoolApplication

    @Mock
    lateinit var getSchoolDetailUseCase: GetSchoolDetailUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        schoolDetailViewModel = SchoolDetailViewModel(application, getSchoolDetailUseCase, contextProvider)
    }

    @Test
    fun `test getSchoolDetails returns success response`() {
        testCoroutineRule.runBlockingTest {
            val school = School("test", "", "", "","")
            val schoolDetail = SchoolDetail("test", "100", "100", "100")
            val result = GetSchoolDetailUseCase.Result.Success(schoolDetail)
            given(getSchoolDetailUseCase.getSchoolDetail(school.id)).willReturn(result)
            schoolDetailViewModel.getSchoolDetails(school)
            assertEquals(schoolDetailViewModel.writingScore.get(), "100")
        }
    }

    @Test
    fun `test getSchoolsDetails returns failure response`() {
        testCoroutineRule.runBlockingTest {
            val school = School("test", "test", "", "", "")
            val result = GetSchoolDetailUseCase.Result.Failure(Exception())
            given(getSchoolDetailUseCase.getSchoolDetail(school.id)).willReturn(result)
            schoolDetailViewModel.getSchoolDetails(school)
            assertEquals(schoolDetailViewModel.successResult.get(), false)

        }
    }
}