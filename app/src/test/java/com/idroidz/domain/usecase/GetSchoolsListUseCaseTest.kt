package com.idroidz.domain.usecase


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.NetworkSchool
import com.idroidz.domain.model.School
import com.idroidz.domain.repository.SchoolRepository
import com.idroidz.nycschools.util.TestCoroutineRule
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

@RunWith(JUnit4::class)
class GetSchoolsListUseCaseTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()


    @Mock
    private lateinit var schoolRepository: SchoolRepository
    private lateinit var getSchoolListUseCase: GetSchoolsListUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getSchoolListUseCase = GetSchoolsListUseCase(schoolRepository)
    }

    @Test
    fun `get schools list with success response`() {
        testCoroutineRule.runBlockingTest {
            val networkSchoolList = mutableListOf<NetworkSchool>()
            networkSchoolList.add(NetworkSchool("", "", "", "", ""))
            val schoolList = mutableListOf<School>()
            schoolList.add(School("", "", "", "", ""))
            val expectedResult = GetSchoolsListUseCase.Result.Success(schoolList)
            given(schoolRepository.getSchoolsList()).willReturn(networkSchoolList)
            val actualResult = getSchoolListUseCase.getSchoolList()
            assertEquals(expectedResult, actualResult)
        }
    }

    @Test
    fun `get schools list with error response`() {
        testCoroutineRule.runBlockingTest {
            val expectedResult = GetSchoolsListUseCase.Result.Success(null)
            given(schoolRepository.getSchoolsList()).willReturn(null)
            val actualResult = getSchoolListUseCase.getSchoolList()
            assertEquals(expectedResult, actualResult)
        }
    }
}