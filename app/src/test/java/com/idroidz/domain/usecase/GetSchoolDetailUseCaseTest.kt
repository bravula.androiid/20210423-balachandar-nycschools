package com.idroidz.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idroidz.domain.model.NetworkSchoolDetail
import com.idroidz.domain.model.SchoolDetail
import com.idroidz.domain.repository.SchoolRepository
import com.idroidz.nycschools.util.TestCoroutineRule
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit


@RunWith(JUnit4::class)
class GetSchoolDetailUseCaseTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()


    @Mock
    private lateinit var schoolRepository: SchoolRepository
    private lateinit var getSchoolDetailUseCase: GetSchoolDetailUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getSchoolDetailUseCase = GetSchoolDetailUseCase(schoolRepository)
    }

    @Test
    fun `get school detail with success response`() {
        testCoroutineRule.runBlockingTest {
            val schoolDetail = SchoolDetail("10", "310", "400", "520")
            val networkSchoolDetail = NetworkSchoolDetail("10", "310", "400", "520")
            val expectedResult = GetSchoolDetailUseCase.Result.Success(schoolDetail)
            given(schoolRepository.getSchoolDetails("21K728")).willReturn(networkSchoolDetail)
            val actualResult = getSchoolDetailUseCase.getSchoolDetail("21K728")
            assertEquals(expectedResult, actualResult)
        }
    }

    @Test
    fun `get school detail with failure response`() {
        testCoroutineRule.runBlockingTest {
            val expectedResult = GetSchoolDetailUseCase.Result.Failure(Exception("Invalid School"))
            given(schoolRepository.getSchoolDetails("21K728")).willAnswer { throw Exception("Invalid School") }
            when (val actualResult: GetSchoolDetailUseCase.Result = getSchoolDetailUseCase.getSchoolDetail("21K728")) {
                is GetSchoolDetailUseCase.Result.Failure -> {
                    assertEquals(expectedResult.throwable.message, actualResult.throwable.message)
                }
            }

        }
    }
}