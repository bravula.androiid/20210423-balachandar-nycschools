package com.idroidz.nycschools

import android.app.Application
import com.idroidz.nycschools.dagger.app.ApplicationComponent
import com.idroidz.nycschools.dagger.app.ApplicationModule
import com.idroidz.nycschools.dagger.app.DaggerApplicationComponent

class SchoolApplication: Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    fun inject() {
        component = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }
}