package com.idroidz.nycschools.dagger.databinding



import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import com.bumptech.glide.Glide


/**
 * File which contains Databinding conversions and adapters
 */

@BindingConversion
fun setVisibility(state: Boolean): Int {
    return if (state) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, drawable: Int?) {
    drawable?.let {
        Glide.with(imageView.context).load(it).into(imageView)
    }
}
