package com.idroidz.nycschools.dagger.app


import com.idroidz.data.ApiManager
import com.idroidz.data.repository.SchoolRepositoryManager
import com.idroidz.domain.repository.SchoolRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/** Dagger module Class to provide instances for repository module.
 */
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideSchoolRepository(apiManager: ApiManager): SchoolRepository {
        return SchoolRepositoryManager(apiManager)
    }

}
