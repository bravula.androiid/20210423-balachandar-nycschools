package com.idroidz.nycschools.dagger.screen

import com.idroidz.nycschools.ui.home.SchoolRouter
import com.idroidz.nycschools.dagger.scope.PerScreen
import com.idroidz.nycschools.ui.base.BaseActivity
import dagger.Module
import dagger.Provides
import java.lang.ref.WeakReference

@Module
class ScreenModule(private val activity: BaseActivity) {

    @PerScreen
    @Provides
    fun providesActivity(): BaseActivity {
        return activity
    }

    @PerScreen
    @Provides
    fun providesSchoolRouter(): SchoolRouter {
        return SchoolRouter(WeakReference(activity))
    }
}
