package com.idroidz.nycschools.dagger.app

import com.idroidz.data.*
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/** Dagger module Class to provide instances for api module.
 * @author Aron
 */
@Module
class ApiModule {

    companion object {
        private const val BASE_URL: String = "https://data.cityofnewyork.us/"
    }

    @Provides
    @Singleton
    fun provideApiManager(apiManager: RetrofitApiManager): ApiManager {
        return apiManager
    }

    @Provides
    @Singleton
    fun provideRetrofitSchoolEndpoint(): RetrofitSchoolEndpoint {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
        val retrofit: Retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
        return retrofit.create(RetrofitSchoolEndpoint::class.java)
    }
}