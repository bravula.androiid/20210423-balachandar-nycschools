package com.idroidz.nycschools.dagger.app



import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.util.CoroutineContextProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/** Dagger module Class to provide instances for app module.
 */
@Module
class ApplicationModule(private val application: SchoolApplication) {

    @Provides
    @Singleton
    fun provideApplication(): SchoolApplication {
        return application
    }

    @Provides
    @Singleton
    fun provideContextProvider(): CoroutineContextProvider {
        return CoroutineContextProvider.ContextProvider()
    }
}