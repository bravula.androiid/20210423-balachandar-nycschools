package com.idroidz.nycschools.dagger.screen

import com.idroidz.nycschools.ui.detail.SchoolDetailActivity
import com.idroidz.nycschools.ui.home.HomeActivity
import com.idroidz.nycschools.dagger.scope.PerScreen
import dagger.Subcomponent

@PerScreen
@Subcomponent(modules = [ScreenModule::class])
interface ScreenComponent {

    fun inject(homeActivity: HomeActivity)

    fun inject(schoolDetailActivity: SchoolDetailActivity)

}
