package com.idroidz.nycschools.dagger.app

import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.dagger.screen.ScreenComponent
import com.idroidz.nycschools.dagger.screen.ScreenModule
import dagger.Component
import javax.inject.Singleton

/** Dagger component to inject dependencies to defined components.
 * @author Aron
 */
@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, ApiModule::class ])
interface ApplicationComponent {

    fun inject(application: SchoolApplication)

    fun plus(screenModule: ScreenModule): ScreenComponent

}