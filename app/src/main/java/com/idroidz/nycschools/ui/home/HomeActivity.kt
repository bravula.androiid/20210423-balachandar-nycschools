package com.idroidz.nycschools.ui.home

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idroidz.nycschools.R
import com.idroidz.nycschools.databinding.ActivityHomeBinding
import com.idroidz.nycschools.ui.base.BaseActivity
import com.idroidz.nycschools.ui.home.adapter.SchoolListAdapter
import javax.inject.Inject

class HomeActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {
    @Inject
    lateinit var schoolViewModel: SchoolViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val binding: ActivityHomeBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_home)

        screenComponent.inject(this)

        binding.viewModel = schoolViewModel
        binding.swipeToRefresh.setOnRefreshListener(this)
        schoolViewModel.getSchools()
    }

    companion object {
        /**
         * bindList uses Databinding to initialize the recyclerView using an ObservableList from the SchoolViewModel
         * this is referenced in activity_home.xml as 'app:adapter={@viewModel}'
         */
        @JvmStatic
        @BindingAdapter("adapter")
        fun bindList(recyclerView: RecyclerView, viewModel: SchoolViewModel) {
            val adapter = SchoolListAdapter(viewModel.schoolsList)
            adapter.onItemClickListener = { viewModel.onSchoolItemClicked(it) }
            recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            recyclerView.adapter = adapter
        }
    }

    /**
     * Overriden method called when swipe down for refresh
     */
    override fun onRefresh() {
        schoolViewModel.getSchools()
    }
}