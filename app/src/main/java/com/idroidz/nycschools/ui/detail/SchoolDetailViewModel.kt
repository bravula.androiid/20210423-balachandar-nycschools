package com.idroidz.nycschools.ui.detail


import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope

import com.idroidz.domain.model.School
import com.idroidz.domain.usecase.GetSchoolDetailUseCase
import com.idroidz.domain.usecase.GetSchoolDetailUseCase.Result.*
import com.idroidz.nycschools.R
import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.ui.base.BaseViewModel
import com.idroidz.nycschools.util.CoroutineContextProvider
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolDetailViewModel @Inject constructor(
        application: SchoolApplication,
        private val getSchoolDetailUseCase: GetSchoolDetailUseCase,
        contextProvider: CoroutineContextProvider
) : BaseViewModel(application, contextProvider) {

    val progressVisible = ObservableBoolean(false)
    val successResult = ObservableBoolean(false)
    val title = ObservableField<String>()
    val mathScore = ObservableField<String>()
    val readingScore = ObservableField<String>()
    val writingScore = ObservableField<String>()
    val description = ObservableField<String>()
    val thumbnail = ObservableField<Int>()

    fun getSchoolDetails(school: School) {
        progressVisible.set(true)
        viewModelScope.launch(getContextProvider().io) {
            val result: GetSchoolDetailUseCase.Result = getSchoolDetailUseCase.getSchoolDetail(school.id)
            progressVisible.set(result == Loading)
            when (result) {
                is Success -> {
                    writingScore.set(result.data.writingScore)
                    readingScore.set(result.data.readingScore)
                    mathScore.set(result.data.mathScore)
                    successResult.set(true)
                    setData(school)
                }
                is Failure -> {
                    setData(school)
                    successResult.set(false)
                }
            }
        }
    }

    private fun setData(school: School) {
        title.set(school.schoolName)
        description.set(school.schoolDescription)
        thumbnail.set(R.drawable.school_detail_thumbnail)
    }
}