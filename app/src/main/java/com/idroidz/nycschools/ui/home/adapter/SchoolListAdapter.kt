package com.idroidz.nycschools.ui.home.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.idroidz.domain.model.School
import com.idroidz.nycschools.R
import com.idroidz.nycschools.databinding.SchoolItemBinding
import com.idroidz.nycschools.ui.home.adapter.SchoolListAdapter.Holder

/**
 * SchoolListAdapter is used to display data for each school in the list
 */
class SchoolListAdapter(schoolList: ObservableList<School>) :
        ObservableRecyclerViewAdapter<School, Holder>(schoolList) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
                SchoolItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                onItemClickListener
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(holder.itemView.context, getItem(position))
    }

    class Holder(
            private val binding: SchoolItemBinding,
            private val onItemClickListener: ((item: Any) -> Unit)?
    ) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(context: Context, school: School) {
            Glide.with(context).load(R.drawable.school_thumbnail).into(binding.schoolThumbnail)
            binding.schoolName.text = school.schoolName
            binding.schoolCity.text = context.getString(R.string.city_text, school.city)
            binding.schoolZipcode.text = context.getString(R.string.zip_text, school.zipCode)
            binding.root.setOnClickListener { onItemClickListener?.invoke(school) }
        }
    }
}
