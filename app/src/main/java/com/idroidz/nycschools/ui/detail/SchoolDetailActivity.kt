package com.idroidz.nycschools.ui.detail

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.idroidz.domain.model.School
import com.idroidz.nycschools.R
import com.idroidz.nycschools.databinding.ActivitySchoolDetailBinding
import com.idroidz.nycschools.ui.base.BaseActivity
import javax.inject.Inject

class SchoolDetailActivity : BaseActivity() {

    @Inject
    lateinit var schoolDetailViewModel: SchoolDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivitySchoolDetailBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_school_detail)

        screenComponent.inject(this)

        binding.viewModel = schoolDetailViewModel
        val school = intent.getSerializableExtra(EXTRA_SCHOOL_DATA) as? School
        school?.let {
            schoolDetailViewModel.getSchoolDetails(school)
        }
        setupActionBar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupActionBar() {
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(android.R.color.transparent)))
        supportActionBar!!.setHomeAsUpIndicator(ContextCompat.getDrawable(applicationContext, R.drawable.custom_back_arrow))
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }


    companion object {
        const val EXTRA_SCHOOL_DATA = "school_data"
    }
}