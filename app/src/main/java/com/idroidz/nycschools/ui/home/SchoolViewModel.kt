package com.idroidz.nycschools.ui.home

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.viewModelScope
import com.idroidz.domain.model.School
import com.idroidz.domain.usecase.GetSchoolsListUseCase
import com.idroidz.domain.usecase.GetSchoolsListUseCase.Result.*
import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.ui.base.BaseViewModel
import com.idroidz.nycschools.ui.detail.SchoolDetailActivity
import com.idroidz.nycschools.util.CoroutineContextProvider
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolViewModel @Inject constructor(
        application: SchoolApplication,
        private val getSchoolsListUseCase: GetSchoolsListUseCase,
        private val schoolRouter: SchoolRouter,
        contextProvider: CoroutineContextProvider
) : BaseViewModel(application, contextProvider) {
    private val TAG = "SchoolViewModel"
    val progressVisible = ObservableBoolean(false)
    val schoolsList = ObservableArrayList<School>()
    val networkFailure = ObservableBoolean(false)

    fun getSchools() {
        progressVisible.set(true)
        viewModelScope.launch(getContextProvider().io) {
            showResult(getSchoolsListUseCase.getSchoolList())
        }

    }

    private fun showResult(result: GetSchoolsListUseCase.Result) {
        viewModelScope.launch(getContextProvider().main) {
            progressVisible.set(result == Loading)
            when (result) {
                is Success -> {
                    result.schoolsList?.let {
                        networkFailure.set(false)
                        schoolsList.addAll(it)
                    }

                }
                is Failure -> {
                    Log.e(TAG, "showResult: ",result.throwable )
                    networkFailure.set(true)
                }
            }
        }
    }

    // Shows school detail screen based on school item clicked
    fun onSchoolItemClicked(school: Any) {
        schoolRouter.navigate(SchoolRouter.Route.SCHOOL_DETAIL, Bundle().apply {
            putSerializable(SchoolDetailActivity.EXTRA_SCHOOL_DATA, (school as School))
        })
    }

}