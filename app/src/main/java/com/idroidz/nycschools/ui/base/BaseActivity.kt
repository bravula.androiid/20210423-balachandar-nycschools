package com.idroidz.nycschools.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.idroidz.nycschools.SchoolApplication
import com.idroidz.nycschools.dagger.app.ApplicationComponent
import com.idroidz.nycschools.dagger.screen.ScreenModule

/** Base activity class which will be inherited by all activities used in the app.
 */
abstract class BaseActivity : AppCompatActivity(){
    val screenComponent by lazy {
        getApplicationComponent().plus(ScreenModule(this))
    }

    private fun getApplicationComponent(): ApplicationComponent {
        return (application as SchoolApplication).component
    }
}