package com.idroidz.nycschools.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.idroidz.nycschools.ui.detail.SchoolDetailActivity
import java.lang.ref.WeakReference

/**
 * SchoolRouter handles navigation for the HomeActivity
 */
class SchoolRouter(private val activityRef: WeakReference<Activity>) {

    enum class Route {
        SCHOOL_DETAIL
    }

    fun navigate(route: Route, bundle:Bundle = Bundle()) {
        when (route) {
            Route.SCHOOL_DETAIL -> { showNextScreen(SchoolDetailActivity::class.java, bundle) }
        }
    }

    private fun showNextScreen(clazz: Class<*>, bundle: Bundle) {
        activityRef.get()?.startActivity(Intent(activityRef.get(), clazz).putExtras(bundle))
    }
}
